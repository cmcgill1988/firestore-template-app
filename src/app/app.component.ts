import { routerAnimation } from './_animations/router.animation';
import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToasterService, ToasterContainerComponent, ToasterConfig } from 'angular2-toaster';
import { NavbarComponent } from './modules/shared-component/navbar/navbar.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  entryComponents: [NavbarComponent],
  animations: [routerAnimation]
})
export class AppComponent {
  constructor() {
  }
  public toasterconfig: ToasterConfig =
  new ToasterConfig({
    positionClass: 'toast-bottom-right',
    tapToDismiss: true,
    timeout: 7000,
    animation: 'fade'
  });

  // change the animation state
  prepRouteState(outlet: any) {
    return outlet.activatedRouteData['animation'] || 'firstPage';
  }
}
