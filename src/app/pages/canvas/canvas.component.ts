import { Component, OnInit } from '@angular/core';
import { CanvasDrawComponent } from './../../modules/canvasDrag/canvasDraw/canvasDraw.component';

@Component({
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss'],
  entryComponents:[CanvasDrawComponent]
})
export class CanvasComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
