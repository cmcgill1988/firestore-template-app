import { Component, OnInit, enableProdMode } from '@angular/core';
import { MasonryGridComponent } from './../../modules/shared-component/masonry-grid/masonry-grid.component';

@Component({
  selector: 'app-masonry',
  templateUrl: './masonry.component.html',
  styleUrls: ['./masonry.component.scss'],
  entryComponents: [MasonryGridComponent]
})
export class MasonryComponent implements OnInit {
items: any[];
  constructor() {
    let defaultImg = 'https://firebasestorage.googleapis.com/v0/b/ng4-template-9fbe1.appspot.com/o/app-assets%2Fplaceholder-thumb.svg?alt=media&token=f4a0883c-b712-4225-bf6e-fe68e1859a87';
    let horImg = 'https://firebasestorage.googleapis.com/v0/b/ng4-template-9fbe1.appspot.com/o/app-assets%2Fsky_banner.jpg?alt=media&token=d52f787e-bf6f-4bc4-8b75-7ac0f7995cd3';
    this.items = [
      { title: 'Hi full card', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is a summary check it out. This is a summary check it out. This is a summary check it out. This is a summary check it out. This is a summary check it out. This is a summary check it out. This is a summary check it out. This is a summary check it out. This is a summary check it out. This is a summary check it out. This is a summary check it out', photoUrl: defaultImg, itemType: "full", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi image', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is an image caption check it out', photoUrl: horImg, itemType: "small-image", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi full card', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is a summary check it out', photoUrl: defaultImg, itemType: "full", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi image', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is an image caption check it out', photoUrl: horImg, itemType: "small-image", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi full card', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is a summary check it out', photoUrl: '', itemType: "full", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi image', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is an image caption check it out', photoUrl: defaultImg, itemType: "image", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi full card', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is a summary check it out', photoUrl: defaultImg, itemType: "full", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi image', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is an image caption check it out', photoUrl: horImg, itemType: "small-image", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi full card', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is a summary check it out', photoUrl: defaultImg, itemType: "full", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi image', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is an image caption check it out', photoUrl: defaultImg, itemType: "image", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi full card', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is a summary check it out', photoUrl: defaultImg, itemType: "full", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi image', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is an image caption check it out', photoUrl: horImg, itemType: "small-image", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi full card', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is a summary check it out', photoUrl: defaultImg, itemType: "full", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi image', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is an image caption check it out', photoUrl: defaultImg, itemType: "image", dateAdded: '2017-10-25T12:52:51.817Z' },
      { title: 'Hi full card', author: "6jy8S90ARTUvuMcyBbTlNbsJBmp1", summary: 'This is a summary check it out', photoUrl: defaultImg, itemType: "full", dateAdded: '2017-10-25T12:52:51.817Z' }
    ]

   }

  ngOnInit() {
  }

}
