import { AuthService } from './../../../providers/auth.service';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/RX';
import { Post } from './../../../data/models/post';

import * as firebase from 'firebase/app';

@Injectable()
export class BlogService {
    constructor(private afs: AngularFirestore, private authService: AuthService) {
    }

    getAllBlogPosts() {
        return this.afs.collection<Post>('blog').snapshotChanges().subscribe();
    }

    getUserBlogPosts(uid: string) {
        let blogRef = this.afs.collection<Post>('blog').doc(uid).collection('posts', ref => ref.where('author', '==', uid).orderBy("timeStamp", "desc"));
        return blogRef.snapshotChanges().map(actions => {
            return actions.map(action => {
                const data = action.payload.doc.data() as Post;
                const id = action.payload.doc.id;
                return { id, data };
            });
        });
    }

    addBlogPost(uid: string, item: Post) {
        var promise = new Promise((resolve, reject) => {
            var currentDate = new Date();
            let userBlogCollection = this.afs.collection('blog').doc(uid).collection('posts');
            userBlogCollection.add({ title: item.title, content: item.content, summary: item.summary, imgUrl: item.imgUrl, author: item.author, likes: item.likes, comments: item.comments, dateAdded: item.dateAdded, timeStamp: firebase.database.ServerValue.TIMESTAMP }).then((response) => {
                resolve(true);
            }).catch((error) => {
                reject(error);
            })
        })
        return promise;
    }

    decrementLikes(postKey: string, author: string, likes: number, uid: any) {
        this.afs.collection('likes').doc('posts').collection(postKey).doc(uid).delete().then(() => {
            return this.afs.collection('blog').doc(uid).collection('posts').doc(postKey).update({ likes: likes })
                .catch((error) => {
                    console.log(error);
                });
        })
    }

    hasLiked(postKey: string, uid: string) {
        const document: AngularFirestoreDocument<any> = this.afs.doc('likes/posts/' + postKey + '/' + uid);
        const document$: Observable<any> = document.valueChanges();
        return document$;
    }

    incrementLikes(postKey: string, author: string, likes: number, uid: any) {
        this.afs.collection('likes').doc('posts').collection(postKey).doc(uid).set({uid: uid}).then(() => {
            return this.afs.collection('blog').doc(uid).collection('posts').doc(postKey).update({ likes: likes })
                .catch((error) => {
                    console.log(error);
                });
        })
    }

    removeItem($key: string, uid: string) {
        this.afs.doc<any>(`likes/${uid}/${$key}`).delete().then(() => {
            this.afs.doc(`comments/${$key}`).delete().then(() => {
                this.afs.doc(`blogItems/${$key}`).delete().then(() => {
                }).catch((error) => {
                    console.log(error);
                })
            }).catch((error) => {
                console.log(error);
            });
        }).catch((error) => {
            console.log(error);
        });
    }
}