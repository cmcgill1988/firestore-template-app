import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { fadeAnimation } from '../../_animations/index';
import { AuthService } from '../../providers/auth.service';
import { BlogService } from './services/blog.service';
import { LikesService } from './../../providers/likes.service';
import { Post } from './../../data/models/post';
import { UserDisplayComponent } from './../../modules/shared-component/user-display/user-display.component';
import { UploadService } from './../../modules/shared-component/uploads/services/upload.service';
import { UploadsComponent } from './../../modules/shared-component/uploads/uploads.component';
import * as _ from 'underscore'
import { CardListComponent } from './../../modules/shared-component/card-list/card-list.component';

@Component({
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
  providers: [BlogService, UploadService],
  animations: [fadeAnimation],
  entryComponents: [UploadsComponent, UserDisplayComponent]
})
export class BlogComponent implements OnInit {
  posts: any;
  currentUser: any;
  authorId: any;
  currentItem: Post;
  showPostControls: Boolean = false;

  constructor(private route: ActivatedRoute, public authService: AuthService, private blogService: BlogService, private likesService: LikesService, public zoneService: NgZone) {
    this.authService.user$.subscribe((response) => {
      this.currentUser = response;
      this.currentItem = this.resetCurrentItem();
      if (response) {
        this.currentItem.author = this.currentUser.uid;
      }
    });
  }

  ngOnInit() {
    let subscription = this.route.params.subscribe(params => {
      this.blogService.getUserBlogPosts(params['uid']).subscribe((data) => {
        this.posts = data.map(item => {
          return {
            id: item.id,
            author: item.data.author,
            comments: item.data.comments,
            content: item.data.content,
            dateAdded: item.data.dateAdded,
            imgUrl: item.data.imgUrl,
            likes: item.data.likes,
            summary: item.data.summary,
            timeStamp: item.data.timeStamp,
            title: item.data.title,
          }
        });
      });
    });
    this.hasLiked('xW6PBLvoBimmdEY36qqw', '6jy8S90ARTUvuMcyBbTlNbsJBmp1');
  }

  addItem(post: Post) {
    this.blogService.addBlogPost(post.author, post).then((response) => {
      this.currentItem = this.resetCurrentItem();
      this.showPostControls = false;
    }).catch((error) => {
      console.log(error);
    });
  }

  addLike(likes: number, author: string, $key: string, uid: string) {
    likes++
    this.likesService.incrementLikes($key, author, likes, uid, 'blog');
  }

  hasLiked(postKey: string, uid: string) {
    return this.likesService.hasLiked(postKey, uid).subscribe(data => {
      return data;
    });
  }

  canPost(): Boolean {
    if (!this.currentItem.title || !this.currentItem.content || !this.currentItem.summary || !this.currentItem.imgUrl) {
      return false;
    } else {
      return true;
    }
  }

  removeLike(likes: number, author: string, $key: string, uid: string) {
    likes--
    this.likesService.decrementLikes($key, author, likes, uid, 'blog');
  }

  removePost($key: string, uid: string) {
    this.blogService.removeItem($key, uid);
  }

  uploadDone(uploadArray: any[]) {
    let uploadIdx = uploadArray.length - 1;
    this.zoneService.run(() => {
      this.currentItem.imgUrl = uploadArray[uploadIdx].url;
    })
  }

  resetCurrentItem(): Post {
    return new Post()
  }
}
