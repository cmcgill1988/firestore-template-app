import { DisplayItem } from './../../data/models/displayItem';
import { Component, OnInit } from '@angular/core';
import { ContentDisplayComponent } from './../../modules/shared-component/content-display/content-display.component';

@Component({
  selector: 'app-about',
  template: `<content-display [content]="content"></content-display>`,
  styleUrls: ['./about.component.scss'],
  entryComponents: [ContentDisplayComponent]
})
export class AboutComponent implements OnInit {
  content: DisplayItem = new DisplayItem('', 'About Us', `<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed tortor vitae tortor porta consectetur. Proin maximus magna ante, ut rutrum elit laoreet at. Nam egestas turpis id mi tristique ornare. Suspendisse felis velit, efficitur ut porta vel, venenatis suscipit velit. Pellentesque nibh ante, vehicula a ex fringilla, consectetur accumsan nisl. Sed sed dapibus odio. Quisque eget purus fringilla, sodales enim vitae, fringilla lacus. Donec auctor ligula quis tincidunt vestibulum. Cras rhoncus nisi eget nunc scelerisque, non varius justo hendrerit. Etiam eu purus feugiat mauris consectetur consequat. Donec lorem libero, interdum non felis ut, sagittis vulputate dui. In finibus tellus sed sem ultricies lobortis. Duis a ultrices mi.
</p>
<p>
Suspendisse sagittis mi sit amet sem fringilla, quis aliquam augue porttitor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus vitae nisl lorem. Cras malesuada quis nibh eu elementum. Sed vehicula neque vitae molestie malesuada. Maecenas vel mauris ante. Aliquam nec sapien fermentum nisl sodales euismod in vitae dolor.
</p>
<p>
Ut eu neque maximus, vulputate nunc id, consectetur odio. Aenean nulla odio, luctus nec blandit vitae, laoreet ut urna. In eget tortor fermentum nulla malesuada aliquam. Fusce non nulla vitae neque convallis auctor. In efficitur purus sed volutpat fermentum. Proin dapibus id nisl vel viverra. Sed suscipit massa sed orci molestie placerat. Maecenas eget consectetur odio. Duis a quam et leo egestas volutpat. Phasellus suscipit rhoncus turpis, ultrices pellentesque ligula mollis quis. Nunc iaculis iaculis faucibus. Morbi laoreet arcu justo, efficitur suscipit urna sagittis a. Duis faucibus gravida ex, et lacinia mi blandit id. Praesent eget magna consectetur, laoreet neque vitae, imperdiet justo. Etiam tincidunt ipsum quis ultrices scelerisque.
</p>
<p>
Sed quis dolor et diam convallis auctor in et quam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed dictum scelerisque blandit. Cras tincidunt massa mi, vitae scelerisque justo imperdiet non. Donec feugiat non nisl quis dignissim. Cras augue nibh, molestie vel vulputate fermentum, faucibus eu mi. Praesent purus ligula, vehicula ac tincidunt a, interdum a risus. Sed et volutpat nibh. Sed et nisl scelerisque, efficitur leo sed, hendrerit orci. Quisque sit amet pellentesque tellus. Nunc feugiat felis neque.
</p>
<p>
Praesent laoreet eu elit non viverra. Sed cursus vulputate orci, quis sollicitudin tellus. Nunc laoreet pretium accumsan. Integer eleifend tempus pellentesque. Cras ac eros eget mi viverra posuere ut vel magna. Nam vitae egestas lacus, vel feugiat erat. Duis sagittis ex quis sem eleifend venenatis. In a felis pretium, laoreet lectus quis, molestie purus. Donec sit amet convallis nibh.
</p>
<p>
In congue volutpat urna, ultrices pulvinar justo. Etiam efficitur nunc diam, non tristique nisi blandit eget. Vestibulum sollicitudin imperdiet neque, nec ornare nulla sodales eu. Morbi et felis et dui vulputate sodales. Nullam ac malesuada urna. Phasellus in condimentum nisl, eu vehicula ipsum. Mauris pulvinar at lacus gravida sodales. Donec ac justo id quam viverra rutrum a sed magna. Quisque sit amet dolor eget enim pretium bibendum at quis nisi. Fusce nec viverra lorem, in pretium ante. Nullam in scelerisque neque, ac bibendum purus. Suspendisse nec dui et tellus commodo pretium in eget magna. Quisque sit amet aliquam neque. Morbi lobortis tortor in ante elementum auctor. Nam at sem velit. Curabitur orci turpis, condimentum vitae elit et, maximus dapibus neque.
</p>
<p>
Ut quis purus feugiat, facilisis dui dignissim, vestibulum elit. Suspendisse nisl lacus, hendrerit sed varius vitae, laoreet sodales erat. Donec a iaculis dui. Sed luctus leo nec ornare egestas. Fusce posuere arcu nunc, at commodo enim elementum vitae. Vivamus quis consectetur nisl. Aliquam at tristique tellus, ut pretium ante. Nullam condimentum sed quam ac pellentesque. Mauris faucibus vitae lacus vitae vulputate. Pellentesque consequat diam eget metus porta hendrerit. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque magna quam, bibendum nec lorem a, fringilla tincidunt orci. Maecenas venenatis ligula ut magna sodales, ac porta justo vulputate. Vestibulum id ante vehicula, auctor mauris nec, pulvinar sapien. Aenean tempus purus purus. Phasellus ac leo tincidunt ligula sodales fermentum.
</p>
<p>
Pellentesque facilisis sollicitudin eros, quis sodales tortor laoreet non. Nullam ligula risus, sollicitudin quis lacinia eu, dictum eget diam. Sed egestas massa dui, vel vulputate ligula bibendum dictum. Phasellus luctus efficitur facilisis. Nunc non erat imperdiet, volutpat ex eget, rhoncus sapien. Nullam ullamcorper elementum nunc ut suscipit. Suspendisse in gravida dolor. Nulla facilisi.
</p>
<p>
Integer eu vulputate quam. Phasellus quis erat nisi. Aenean quam libero, accumsan non purus sit amet, dapibus gravida purus. Praesent pretium commodo leo, a eleifend lacus viverra quis. Donec vitae nisi congue, luctus arcu nec, viverra nisl. Integer vehicula purus vitae rutrum luctus. Donec mattis orci massa, eget accumsan nunc dapibus nec. Donec ac congue arcu. In vel pulvinar erat. Aenean sed interdum ante. Curabitur vitae diam dolor. Pellentesque vitae lacus pellentesque, porta metus at, pharetra sapien. Aliquam et est sed mi elementum placerat in non elit. Suspendisse potenti.
</p>
<p>
Sed blandit erat sit amet quam ornare laoreet. Nulla vitae leo vitae dui tristique facilisis. Morbi et leo nunc. Integer maximus quam sapien, vitae pretium tellus fringilla at. Interdum et malesuada fames ac ante ipsum primis in faucibus. Phasellus faucibus dui et feugiat ornare. Donec neque dui, posuere in imperdiet non, tincidunt sit amet velit. Pellentesque faucibus, enim ut gravida pulvinar, orci mi ornare neque, ac cursus erat ante ut eros. Suspendisse in maximus orci, non auctor tortor. Nam et diam sed tellus rutrum pulvinar. Maecenas vitae eros faucibus, gravida odio in, bibendum quam. Quisque laoreet erat a urna sagittis scelerisque. Sed quis placerat nibh. Aenean dignissim diam eros, quis gravida ex elementum id. Aenean auctor suscipit turpis eget sodales.
</p>`, 'Lorem Ipsum est.', 'https://firebasestorage.googleapis.com/v0/b/ng4-template-9fbe1.appspot.com/o/app-assets%2Fsky_banner.jpg?alt=media&token=d52f787e-bf6f-4bc4-8b75-7ac0f7995cd3','12345',0,0,'aboutPage','',new Date());
  constructor() { }

  ngOnInit() {
  }

}
