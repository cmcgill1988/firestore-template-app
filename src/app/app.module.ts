//Angular modules
import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';


//Third party modules
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'
import { CollapseModule } from 'ngx-bootstrap';
import { NgbModule, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ToasterModule } from 'angular2-toaster';
import { DragulaModule } from 'ng2-dragula'

//Third party libraries
import * as lo from "lodash";
import * as _ from "underscore";
import * as $ from 'jquery';
import 'hammerjs';
import 'hammer-timejs';

//App Config Files
import { FirebaseConfig } from './config/firebaseConfig';

//App libraries
import * as _animations from './_animations/index';

// App modules
import { AdminModule } from './modules/admin/admin.module';
import { CanvasDragModule } from './modules/canvasDrag/canvasDrag.module';
import { SharedComponentModule } from './modules/shared-component/shared-component.module';
import { SharedDirectivesModule } from './modules/shared-directives/shared-directives.module';
import { RoutingModule } from './modules/routing/routing.module';
//App Providers
import { AuthService } from './providers/auth.service';
import { LikesService } from './providers/likes.service';

//App Pages
import { AppComponent } from './app.component';
import { AboutComponent } from './pages/about/about.component';
import { BlogComponent } from './pages/blog/blog.component';
import { CanvasComponent } from './pages/canvas/canvas.component';
import { HomeComponent } from './pages/home/home.component';
import { MasonryComponent } from './pages/masonry/masonry.component';

//App components
import { UserDisplayComponent } from './modules/shared-component/user-display/user-display.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    BlogComponent,
    CanvasComponent,
    HomeComponent,
    MasonryComponent
  ],
  imports: [
    BrowserModule,
    AdminModule,
    Angular2FontawesomeModule,
    AngularFireModule.initializeApp(FirebaseConfig),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserAnimationsModule,
    BootstrapModalModule,
    CanvasDragModule,
    CollapseModule,
    DragulaModule,
    FormsModule,
    InfiniteScrollModule,
    NgbModule.forRoot(),
    RoutingModule,
    SharedComponentModule,
    SharedDirectivesModule,
    ToasterModule
  ],
  providers: [AuthService, LikesService, NgbDropdownConfig, HammerGestureConfig],
  bootstrap: [AppComponent]
})
export class AppModule { }
