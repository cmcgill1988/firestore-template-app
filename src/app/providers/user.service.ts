import { User } from './../data/models/user';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {
    private usersCollection: AngularFirestoreCollection<User>;
    users$: Observable<User[]>;
    constructor(public afireauth: AngularFireAuth, public afs: AngularFirestore) {
        this.usersCollection = this.afs.collection<User>('users');
        this.users$ = this.usersCollection.valueChanges();
     }

    addUser(newUser) {
        var promise = new Promise((resolve, reject) => {
            this.afireauth.auth.createUserWithEmailAndPassword(newUser.email, newUser.password).then(() => {
                this.afireauth.auth.currentUser.updateProfile({
                    displayName: newUser.displayName,
                    photoURL: ''
                }).then(() => {
                    this.usersCollection.doc(this.afireauth.auth.currentUser.uid).set({
                        $key: this.afireauth.auth.currentUser.uid,
                        uid: this.afireauth.auth.currentUser.uid,
                        displayName: newUser.displayName,
                        photoURL: ''
                    }).then(() => {
                        resolve({ success: true });
                    }).catch((err) => {
                        reject(err);
                    })
                }).catch((err) => {
                    reject(err);
                })
            }).catch((err) => {
                reject(err);
            })
        })
        return promise;
    }

    passwordReset(email: string) {
        var promise = new Promise((resolve, reject) => {
            this.afireauth.auth.sendPasswordResetEmail(email).then(() => {
                resolve({ success: true });
            }).catch((err) => {
                reject(err);
            })
        })
        return promise;
    }

    updateImage(imageUrl: string) {
        var promise = new Promise((resolve, reject) => {
            this.afireauth.auth.currentUser.updateProfile({
                displayName: this.afireauth.auth.currentUser.displayName,
                photoURL: imageUrl
            }).then(() => {
                this.afs.doc<User>(`users/${this.afireauth.auth.currentUser.uid}`).update({
                    photoURL: imageUrl
                }).then(() => {
                    resolve({ success: true });
                }).catch((err) => {
                    reject(err);
                })
            }).catch((err) => {
                reject(err);
            })
        })
        return promise;
    }

    getSingleUserDetails(uid: string) {
        var userObj;
        var promise = new Promise((resolve, reject)=>{
            this.afs.collection('users').doc(uid).valueChanges().subscribe((data) => {
                userObj = data;
                resolve(data);
            })
        })
        return promise;
    }

    updateDisplayName(newName: string) {
        var promise = new Promise((resolve, reject) => {
            this.afireauth.auth.currentUser.updateProfile({
                displayName: newName,
                photoURL: this.afireauth.auth.currentUser.photoURL
            }).then(() => {
                this.afs.doc<User>(`users/${this.afireauth.auth.currentUser.uid}`).update({
                    displayName: newName
                }).then(() => {
                    resolve({ success: true });
                }).catch((err) => {
                    reject(err);
                })
            }).catch((err) => {
                reject(err);
            })
        })
        return promise;
    }

    getAllUsers() {
        return this.afs.collection<User>('users').valueChanges().subscribe();
    }
}