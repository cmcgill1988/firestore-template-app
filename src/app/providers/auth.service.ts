import { Injectable, EventEmitter } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs'
import { UserCredentials } from './../data/interfaces/userCredentials';


@Injectable()
export class AuthService {
    user$: Observable<any>;
    loginStateChange$: EventEmitter<any>;

    constructor(public afireauth: AngularFireAuth) {
        this.loginStateChange$ = new EventEmitter();
        this.user$ = this.afireauth.authState;
        this.user$.subscribe((response) => { });
    }

    login(credentials: UserCredentials) {
        var promise = new Promise((resolve, reject) => {
            this.afireauth.auth.signInWithEmailAndPassword(credentials.email, credentials.password).then(() => {
                resolve(true);
            }).catch((err) => {
                reject(err);
            })
        })
        return promise;
    }

    logout() {
        var promise = new Promise((resolve, reject) => {
            this.afireauth.auth.signOut().then(() => {
                resolve(true)
            }).catch((err) => reject(err));
        });
        return promise;
    }

}