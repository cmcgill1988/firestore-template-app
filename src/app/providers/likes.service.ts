import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/RX';
import * as firebase from 'firebase/app';
import { Post } from './../data/models/post';
import { AuthService } from './auth.service';

@Injectable()
export class LikesService {
    constructor(private afs: AngularFirestore, private authService: AuthService) {
    }

    decrementLikes(postKey: string, author: string, likes: number, uid: string, likeNode: string) {
        this.afs.collection('likes').doc('posts').collection(postKey).doc(uid).delete().then(() => {
            return this.afs.collection(likeNode).doc(uid).collection('posts').doc(postKey).update({ likes: likes })
                .catch((error) => {
                    console.log(error);
                });
        })
    }

    hasLiked(postKey: string, uid: string) {
        const document: AngularFirestoreDocument<any> = this.afs.doc('likes/posts/' + postKey + '/' + uid);
        const document$: Observable<any> = document.valueChanges()
        return document$;
    }

    incrementLikes(postKey: string, author: string, likes: number, uid: string, likeNode: string) {
        console.log('liking');
        this.afs.collection('likes').doc('posts').collection(postKey).doc(uid).set({ uid: uid }).then(() => {
            return this.afs.collection(likeNode).doc(uid).collection('posts').doc(postKey).update({ likes: likes })
                .catch((error) => {
                    console.log(error);
                });
        })
    }
}