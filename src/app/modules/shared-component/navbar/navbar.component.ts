import { AvatarComponent } from './../avatar/avatar.component';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../providers/auth.service';
import { ToasterService } from 'angular2-toaster';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  entryComponents: [AvatarComponent]
})

export class NavbarComponent implements OnInit {
  authenticated: any;
  currentUser: any;
  isCollapsed: boolean = false;
  constructor(public authService: AuthService, private toasterService: ToasterService) {
    this.authService.user$.subscribe((response) => {
      this.currentUser = response;
    });

    if(window.screen.width < 800){
      this.isCollapsed = true;
    }
  }

  ngOnInit() {
  }

  logout() {
    this.authService.logout().then((response) => {
      this.authenticated = false;
      this.toasterService.pop('success', 'Success', 'Logout Successful');
    }).catch((error) => {
      this.toasterService.pop('error', 'Error', 'Logout failed');
    });
  }
}
