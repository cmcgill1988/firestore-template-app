import { Component, OnInit, Input } from '@angular/core';
import { BlogService } from './../../../../pages/blog/services/blog.service';

@Component({
  selector: 'card-item-post-controls',
  templateUrl: './card-item-post-controls.component.html',
  styleUrls: ['./card-item-post-controls.component.scss']
})
export class CardItemPostControlsComponent implements OnInit {
  @Input() uid: string;
  @Input() postAuthor: string;
  @Input() postId: string;
  constructor(private blogService: BlogService) { }

  ngOnInit() {
  }

  canShow() {
    return this.postAuthor === this.uid;
  }

  removePost() {
    this.blogService.removeItem(this.postId, this.postAuthor);
  }

}
