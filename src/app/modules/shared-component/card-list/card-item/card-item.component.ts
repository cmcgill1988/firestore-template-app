import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { LikesService } from './../../../../providers/likes.service';
import { Post } from './../../../../data/models/post';
import { User } from './../../../../data/models/user';
import { Observable } from 'rxjs/Observable';
import { UserDisplayComponent } from './../../user-display/user-display.component';
import { fadeAnimation } from './../../../../_animations/fade.animation';

@Component({
  selector: 'card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss'],
  entryComponents: [UserDisplayComponent],
  animations:[fadeAnimation]
})
export class CardItemComponent implements OnInit, OnChanges {
@Input() item: Post;
@Input() currentUser: string;
@Input() originNode: string;
editing: boolean;
selectedItem: string;
  constructor() { }
  ngOnInit() { }
  ngOnChanges(changes:SimpleChanges){
    if(changes){
      this.item = changes.item.currentValue;
      this.currentUser = changes.currentUser.currentValue;
      this.currentUser = changes.currentUser.currentValue;

    }
  }
}
