import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/RX';
import { AuthService } from './../../../providers/auth.service';
import { Post } from './../../../data/models/post';
import { User } from './../../../data/models/user';
import { CardItemComponent } from './card-item/card-item.component';

@Component({
  selector: 'card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss'],
  entryComponents: [CardItemComponent]
})
export class CardListComponent implements OnInit, OnChanges {
@Input() items: any[];
@Input() searchEnabled: boolean = false;
@Input() originNode: string;

filteredItems: any[]
currentUser: User;
constructor(public authService: AuthService) {
  this.filteredItems = this.items;
  this.authService.user$.subscribe((response) => {
    this.currentUser = response;
  });
 }

  ngOnInit() {
  }

ngOnChanges(changes: SimpleChanges){
  if(changes){
    this.filteredItems = this.items;
  }
}

  filterItems(searchVal: string) {
    this.filteredItems = this.items.filter(item => {
      return item.title.includes(searchVal) && item.summary.includes(searchVal);
    });
  }

}
