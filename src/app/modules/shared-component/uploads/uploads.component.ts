import { Component, Output, EventEmitter, Input } from '@angular/core';
import { UserService } from './../../../providers/user.service';
import { UploadService } from './services/upload.service';
import { Upload } from './../../../data/models/upload';

import * as lo from "lodash";
import { Observable } from 'rxjs';

@Component({
  selector: 'upload-form',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.scss'],
  providers: [UploadService, UserService]
})
export class UploadsComponent {
  @Input() uploadType: string;
  @Output() currentUpload: Upload;
  @Output() uploadDone = new EventEmitter<any[]>();
  uploadList: any = [];
  dropZoneActive: boolean;
  selectedFiles: FileList;
  constructor(private upSvc: UploadService) {
    this.dropZoneActive = false;
   }

  detectFiles(event) {
    this.handleDrop(event.target.files);
  }

  dropZoneState($event: boolean) {
    this.dropZoneActive = $event;
  }

  handleDrop(fileList: FileList) {
    this.selectedFiles = fileList;
    const filesIndex = lo.range(fileList.length);
    lo.each(filesIndex, (idx) => {
      this.currentUpload = new Upload(fileList[idx]);
      switch (this.uploadType) {
        case 'profile':
          this.upSvc.pushProfileUpload(this.currentUpload).then((response) => {
            this.uploadList.push(response);
            if (idx === fileList.length - 1) {
              this.uploadDone.emit(this.uploadList);
              this.currentUpload = undefined;
            }
          }).catch((error) => {
            console.log(error);
          });
          break;
        default:
          this.upSvc.pushUpload(this.currentUpload).then((response) => {
            this.uploadList.push(response);
            if (idx === fileList.length - 1) {
              this.uploadDone.emit(this.uploadList);
            }
          }).catch((error) => {
            console.log(error);
          });
      }
    });
  }
}
