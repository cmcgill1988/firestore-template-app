import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { UserService } from './../../../../providers/user.service';
import { Upload } from './../../../../data/models/upload';


@Injectable()
export class UploadService {
    uploads: AngularFirestoreCollection<Upload>;

    constructor(private afs: AngularFirestore, private af: AngularFireAuth, public userService: UserService) { }

    deleteUpload(upload: Upload) {
        this.deleteFileData(upload.$key)
            .then(() => {
                this.deleteFileStorage(upload.name)
            })
            .catch(error => console.log(error))
    }

    getUploads(query) {
        return this.afs.collection<Upload>('uploads').valueChanges().subscribe();
    }

    pushUpload(upload: Upload) {
        var promise = new Promise((resolve, reject) => {
            let storageRef = firebase.storage().ref();
            let dateIdent = new Date();
            let uploadTask = storageRef.child(`uploads/${this.af.auth.currentUser.uid}/${upload.file.name}_${dateIdent}`).put(upload.file);
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {
                    upload.progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100
                },
                (error) => {
                    reject(error);
                    console.log(error)
                },
                () => {
                    upload.url = uploadTask.snapshot.downloadURL
                    upload.name = upload.file.name
                    this.saveFileData(upload)
                    resolve(upload);
                }
            );

        })
        return promise;
    }

    pushProfileUpload(upload: Upload) {
        var promise = new Promise((resolve, reject) => {
            let storageRef = firebase.storage().ref();
            let dateIdent = new Date();
            let uploadTask = storageRef.child(`profiles/${this.af.auth.currentUser.uid}/${this.af.auth.currentUser.uid}`).put(upload.file);
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {
                    upload.progress = (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100
                },
                (error) => {
                    reject(error);
                    console.log(error)
                },
                () => {
                    upload.url = uploadTask.snapshot.downloadURL
                    upload.name = upload.file.name
                    this.saveFileData(upload)
                    this.userService.updateImage(upload.url);
                    resolve(upload);
                }
            );

        })
        return promise;
    }


    private deleteFileData(key: string) {
        return this.afs.collection('uploads').doc(key).delete();
    }

    private deleteFileStorage(name: string) {
        let storageRef = firebase.storage().ref();
        storageRef.child(`uploads/${name}`).delete()
    }

    private saveFileData(upload: Upload) {
        this.afs.collection('uploads').add({name:upload.name, url: upload.url, uid: this.af.auth.currentUser.uid});
    }

}