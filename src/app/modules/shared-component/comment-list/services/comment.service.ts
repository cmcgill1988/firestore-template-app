import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/RX';
import * as firebase from 'firebase/app';

import { User } from './../../../../data/models/user';
import { Comment } from './../../../../data/models/comment';

@Injectable()
export class CommentService {
    constructor(private afs: AngularFirestore, ) {
    }

    addComment(comment: Comment, postAuthor: string, comments: number) {
        var currentDate = new Date();

        var promise = new Promise((resolve, reject) => {
            this.afs.collection('comments').doc(comment.parentPost).collection('comments').add({
                content: comment.content,
                author: comment.author,
                parentPost: comment.parentPost,
                commentNode: comment.commentNode,
                likes: 0,
                replies: 0,
                dateAdded: comment.dateAdded,
                timeStamp: firebase.database.ServerValue.TIMESTAMP
            }).then(() => {
                this.afs.collection(comment.commentNode).doc(postAuthor).collection('posts').doc(comment.parentPost).update({ comments: comments })
                    .then(() => {
                        resolve(true)
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            }).catch((error) => {
                console.log(error);
            });
        })
        return promise;
    }

    getComments(parentId: string) {
        const commentRef = this.afs.collection<Comment>('comments').doc(parentId).collection('comments', ref => ref.orderBy("dateAdded", "asc"));
        const comments$ = commentRef.valueChanges();
        return comments$;
    }
}