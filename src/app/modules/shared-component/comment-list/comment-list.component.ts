import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import { AuthService } from './../../../providers/auth.service';
import { CommentService } from './services/comment.service';
import { UserService } from './../../../providers/user.service';
import { Comment } from './../../../data/models/comment';
import { User } from './../../../data/models/user';

import { fadeAnimation } from './../../../_animations/fade.animation';

import { CommentControlsComponent } from './comment-controls/comment-controls.component';
import { CommentComponent } from './comment/comment.component';
import { UserDisplayComponent } from './../user-display/user-display.component';
import { Post } from './../../../data/models/post';

@Component({
  selector: 'comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss'],
  entryComponents: [CommentComponent, CommentControlsComponent, UserDisplayComponent],
  providers: [CommentService, UserService],
  animations: [fadeAnimation]
})
export class CommentListComponent implements OnInit, OnChanges {
  @Input() parent: any;
  @Input() commentNode: string;
  currentUser: User;
  comments: any;
  constructor(public authService: AuthService, public commentService: CommentService) {
    this.authService.user$.subscribe((response) => {
      this.currentUser = response;
    });
  }

  ngOnInit() {
  }

  ngOnChanges(changes:SimpleChanges){
    if(changes){
      this.parent = changes.parent.currentValue;
      this.commentNode = changes.commentNode.currentValue;
      this.comments = this.commentService.getComments(this.parent.id);
    }
  }

}
