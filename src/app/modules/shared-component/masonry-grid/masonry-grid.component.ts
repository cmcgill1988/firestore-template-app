import { Observable } from 'rxjs/RX';
import { fadeDropAnimation } from './../../../_animations/index';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MasonryGridItemComponent } from './masonry-grid-item/masonry-grid-item.component';

@Component({
  selector: 'masonry-grid',
  templateUrl: './masonry-grid.component.html',
  styleUrls: ['./masonry-grid.component.scss'],
  animations: [fadeDropAnimation],
  entryComponents: [MasonryGridItemComponent]
})
export class MasonryGridComponent implements OnInit, OnChanges {
  @Input() items: any;
  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      this.items = changes.items.currentValue;
    }
  }

}
