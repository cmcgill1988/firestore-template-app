import { Observable } from 'rxjs';
import { fadeAnimation } from './../../../../_animations/index';
import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { UserDisplayComponent } from './../../user-display/user-display.component';

@Component({
  selector: 'masonry-grid-item',
  templateUrl: './masonry-grid-item.component.html',
  styleUrls: ['./masonry-grid-item.component.scss'],
  entryComponents:[UserDisplayComponent],
  animations: [fadeAnimation]
})
export class MasonryGridItemComponent implements OnInit, OnChanges {
@Input() item: any;
@Input() itemType: any;
  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges){
    if(changes){
      this.item = changes.item.currentValue;
      this.itemType = changes.itemType.currentValue;
    }
  }

}
