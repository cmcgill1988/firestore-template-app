import { User } from './../../../data/models/user';
import { UserService } from './../../../providers/user.service';
import { AvatarComponent } from './../avatar/avatar.component';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'user-display',
  templateUrl: './user-display.component.html',
  styleUrls: ['./user-display.component.scss'],
  entryComponents: [AvatarComponent],
  providers: [UserService]
})
export class UserDisplayComponent implements OnInit {
  @Input() uid: string;
  @Input() avatarSize: string;
  user: User;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getSingleUserDetails(this.uid).then((response: User) => {
      this.user = response;
    }).catch((error) => {
      console.log(error);
    })
  }

}
