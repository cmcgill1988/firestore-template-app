import { Component, ViewChild, Renderer, OnInit } from '@angular/core';

@Component({
  selector: 'canvas-draw',
  templateUrl: './canvasDraw.component.html',
  styleUrls: ['./canvasDraw.component.scss']
})
export class CanvasDrawComponent implements OnInit {
  @ViewChild('myCanvas') canvas: any;
  canvasX: number = 25;
  canvasY: number = 25;
  boxDim: number = 32;
  subSection: number = 0;

  canvasWidth: number = 25;
  canvasHeight: number = 25;

  gridColor: string = '#000';

  canvasElement: any;
  lastX: number;
  lastY: number;

  currentColour: string = '#1abc9c';
  availableColours: any;

  brushSize: number = 10;

  constructor(public renderer: Renderer) {
    this.availableColours = [
      '#1abc9c',
      '#3498db',
      '#9b59b6',
      '#e67e22',
      '#e74c3c'
    ];
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.drawCanvas();
  }

  changeColour(colour) {
    this.currentColour = colour;
  }

  changeSize(size) {
    this.brushSize = size;
  }

  drawCanvas() {
    this.canvasElement = this.canvas.nativeElement;
    this.canvasHeight = this.canvasY * this.boxDim;
    this.canvasWidth = this.canvasX * this.boxDim;

    if (this.canvasWidth > 0 && this.boxDim > 0) {
      this.renderer.setElementAttribute(this.canvasElement, 'width', this.canvasWidth + '');
    } else {
      this.renderer.setElementAttribute(this.canvasElement, 'width', (window.screen.width - ((window.screen.width / 100) * 2)) + '');
    }

    if (this.canvasHeight > 0 && this.boxDim > 0) {
      this.renderer.setElementAttribute(this.canvasElement, 'height', this.canvasHeight + '');
    } else {
      this.renderer.setElementAttribute(this.canvasElement, 'height', (window.screen.height - ((window.screen.width / 100) * 2)) + '');
    }
    this.drawGrid();
  }

  drawGrid() {
    if (this.boxDim > 0) {
      let ctx = this.canvasElement.getContext("2d");
      ctx.globalCompositeOperation = 'source-over';

      var Height = this.canvasElement.height;
      var Width = this.canvasElement.width;
      ctx.lineWidth = 1
      ctx.strokeStyle = 'rgba(0,0,0,0.5)';
      ctx.lineJoin = "round";

      for (let i = 0; i < Height + 1; i += this.boxDim) {
        ctx.moveTo(0, i);
        ctx.lineTo(Width, i);
        ctx.stroke();
      }
      for (let i = 0; i < Width + 1; i += this.boxDim) {
        ctx.moveTo(i, 0);
        ctx.lineTo(i, Height);
        ctx.stroke();
      }

      if (this.subSection > 1) {
        ctx.lineWidth = 1;
        ctx.strokeStyle = 'rgba(0,0,0,0.25)';
        for (let i = 0; i < Height; i += this.boxDim) {
          ctx.setLineDash([this.boxDim / 5]);
          ctx.moveTo(0, i);
          ctx.lineTo(Width, i);
          ctx.stroke();
        }
        for (let i = 0; i < Width; i += this.boxDim) {
          ctx.setLineDash([this.boxDim / 5]);
          ctx.moveTo(i, 0);
          ctx.lineTo(i, Height);
          ctx.stroke();
        }
      }
    }
  }

  handleStart(ev) {
    this.lastX = ev.changedPointers[0].layerX;
    this.lastY = ev.changedPointers[0].layerY;
  }

  handleMove(ev) {
    let ctx = this.canvasElement.getContext('2d');

    let currentX = ev.changedPointers[0].layerX;
    let currentY = ev.changedPointers[0].layerY;

    ctx.beginPath();
    ctx.lineJoin = "round";
    ctx.moveTo(this.lastX, this.lastY);
    ctx.lineTo(currentX, currentY);
    ctx.closePath();
    ctx.strokeStyle = this.currentColour;
    ctx.lineWidth = this.brushSize;
    ctx.stroke();

    this.lastX = currentX;
    this.lastY = currentY;
  }

  clearCanvas() {
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    this.drawGrid();
  }

}