import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'canvas-options',
  templateUrl: './canvas-options-modal.component.html',
  styleUrls: ['./canvas-options-modal.component.scss']
})
export class CanvasOptionsModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
