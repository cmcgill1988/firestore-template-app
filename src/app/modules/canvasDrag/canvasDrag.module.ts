import { SharedDirectivesModule } from './../shared-directives/shared-directives.module';
import { SharedComponentModule } from './../shared-component/shared-component.module';
import { FormsModule } from '@angular/forms';

import { NgbModule, NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CanvasDrawComponent } from './canvasDraw/canvasDraw.component';
import { CanvasOptionsModalComponent } from './canvas-options-modal/canvas-options-modal.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    SharedDirectivesModule,
    SharedComponentModule
  ],
  declarations: [CanvasDrawComponent,
    CanvasOptionsModalComponent
  ],
  exports: [CanvasDrawComponent]
})
export class CanvasDragModule { }