import { SharedDirectivesModule } from './../shared-directives/shared-directives.module';
import { AvatarComponent } from './../shared-component/avatar/avatar.component';
import { Router, ActivatedRoute, RouterModule } from '@angular/router';
import { ModalService } from './../shared-component/modal/services/modal.service';
import { SharedComponentModule } from './../shared-component/shared-component.module';
import { ErrorComponent } from './../routing/error/error.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdminComponent } from './admin.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ProfileComponent } from './profile/profile.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedComponentModule,
    SharedDirectivesModule
  ],
  declarations: [
    AdminComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent
],
  exports: [
    AdminComponent,
    ProfileComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent],
    providers:[ModalService]
})
export class AdminModule { }