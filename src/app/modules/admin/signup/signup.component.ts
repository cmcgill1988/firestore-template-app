import { AuthService } from './../../../providers/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';

import { UserService } from './../../../providers/user.service';
import { fadeAnimation } from './../../../_animations/fade.animation';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers: [UserService, AuthService],
  animations: [fadeAnimation]
})
export class SignupComponent implements OnInit {
  newUser = {
    email: '',
    password: '',
    displayName: ''
  };

  isStrongPass: Boolean = false;

  constructor(public auth: AuthService, public router: Router, public userService: UserService, private toasterService: ToasterService) { }

  ngOnInit() {
  }

  signUp() {
    if (this.newUser.email == '' || this.newUser.password == '' || this.newUser.displayName == '') {
      this.toasterService.pop('error', 'Error', 'All fields are required. Display Name can be changed after login.');
    }
    else if (!this.strongPassword(this.newUser.password)) {
      let strongPassMsg = 'Password is not strong enough.';
      this.toasterService.pop('error', 'Error', strongPassMsg);
    }
    else {
      this.userService.addUser(this.newUser).then((res: any) => {
        if (res.success) {
          this.auth.user$.subscribe((response) => {
            let currentUser = response;
            this.router.navigate(['/profile', currentUser.uid]);
          });
        } else {
          this.toasterService.pop('error', 'Error', res);
        }
      })
    }
  }

  strongPassword(pass: string) {
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    if (strongRegex.test(pass)) {
      this.isStrongPass = true
    }
    return this.isStrongPass;
  }
}
