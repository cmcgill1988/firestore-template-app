import { Observable } from 'rxjs';
import { Component, OnInit, EventEmitter, NgZone } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { AuthService } from './../../../providers/auth.service';
import { ModalService } from './../../shared-component/modal/services/modal.service';
import { UserService } from './../../../providers/user.service';
import { UploadService } from './../../shared-component/uploads/services/upload.service';
import { AvatarComponent } from './../../shared-component/avatar/avatar.component';
import { ModalComponent } from './../../shared-component/modal/modal.component';
import { UploadsComponent } from './../../shared-component/uploads/uploads.component';
import { fadeAnimation } from './../../../_animations/fade.animation';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  entryComponents: [AvatarComponent, ModalComponent],
  animations: [fadeAnimation],
  providers: [UserService, UploadService, ModalService]
})
export class ProfileComponent implements OnInit {
  user: any;
  oldName: string;
  closeResult: any;
  imgUrl: string;
  constructor(
    private modalService: ModalService,
    private route: ActivatedRoute,
    private router: Router,
    public userService: UserService,
    public uploadService: UploadService,
    private toasterService: ToasterService,
    public zoneService: NgZone) {
  }
  ngOnInit() {
    this.route.params
      .map(params => params['uid'])
      .switchMap(uid => this.userService.getSingleUserDetails(uid))
      .map(response => {
        this.user = response;
        console.log(this.user);
        this.oldName = this.user.displayName;
      })
      .subscribe();
  }

  editImage(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }


  editName(name: string) {
    if (name.length === 0){
      this.toasterService.pop('error', 'Error', 'Display name must not be empty.');
    } else if(this.oldName !== name) {
      this.userService.updateDisplayName(name).then(() => {
        this.oldName = name;
        this.toasterService.pop('success', 'Success', 'Display name successfully changed');
      }).catch((error) => {
        this.toasterService.pop('error', 'Error', error);
      });
    }
  }

  uploadDone(uploadArray: any[]) {
    let uploadIdx = uploadArray.length - 1;
    this.zoneService.run(() => {
      this.user.photoURL = uploadArray[uploadIdx].url;
    })
    this.closeModal('profile-pic-upload');
  }
}
