import { LoginComponent } from './../admin/login/login.component';
import { ProfileComponent } from './../admin/profile/profile.component';
import { AdminComponent } from './../admin/admin.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AboutComponent } from './../../pages/about/about.component';
import { BlogComponent } from './../../pages/blog/blog.component';
import { CanvasComponent } from './../../pages/canvas/canvas.component';
import { HomeComponent } from './../../pages/home/home.component';
import { ErrorComponent } from './error/error.component';
import { SignupComponent } from '../admin/signup/signup.component';
import { MasonryComponent } from './../../pages/masonry/masonry.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot([
      {
        path: '', component: HomeComponent, data: {
          animation: 'homePage'
        }
      },
      {
        path: 'about', component: AboutComponent, data: {
          animation: 'aboutPage'
        }
      },
      {
        path: 'admin', component: AdminComponent, data: {
          animation: 'adminPage'
        }
      },
      {
        path: 'profile/:uid', component: ProfileComponent, data: {
          animation: 'profilePage'
        }
      },
      {
        path: 'login', component: LoginComponent, data: {
          animation: 'loginPage'
        }
      },
      {
        path: 'sign-up', component: SignupComponent, data: {
          animation: 'signUpPage'
        }
      },
      {
        path: 'blog/:uid', component: BlogComponent, data: {
          animation: 'blogPage'
        }
      },
      {
        path: 'canvas', component: CanvasComponent, data: {
          animation: 'canvasPage'
        }
      },
      {
        path: 'masonry', component: MasonryComponent, data: {
          animation: 'masonryPage'
        }
      },
      {
        path: '**', component: ErrorComponent,
        data: {
          animation: 'errorPage'
        }
      }
    ])
  ],
  declarations: [ErrorComponent],
  exports: [RouterModule]
}
)
export class RoutingModule { }